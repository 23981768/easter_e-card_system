
let inputClientname = document.querySelector("#clientName");
let inputBusinesstname = document.querySelector("#businessName");
let sender = document.querySelector("#sender");
let reciever = document.querySelector("#reciever");
let cardSender = document.querySelector("#Cardsender");
let cardReciever = document.querySelector("#Cardreciever");

console.log(localStorage);

function test(){
    localStorage.setItem("clientName", inputClientname.value);
    localStorage.setItem("businessName", inputBusinesstname.value);

    console.log(localStorage);

    reciever.innerHTML = localStorage.getItem("clientName");
    sender.innerHTML = localStorage.getItem("businessName");
    
    
}

var tl = gsap.timeline({
    
    repeatDelay: 1.5
});
    //Positioning certain elements
    tl.to(".egg", {y: 10, x:-78})
      .to(".rabbit1", {y: 60, x: 350, scale:0.5, duration:0})
      .to(".rabbit2", 0.6, {opacity:0, y:30, x:-60})
      .to(".paper", 0, {opacity:0, y:100, x:140})
      .to(".eggbasket", 0, {opacity: 0, y:95, x:-110})
      .to(".note", 0, {opacity:0, x:-60, y: -95})

      //Animation begins here

      //Egg falls out of Basket
      .to(".basket", 0, {opacity: 1, x: -110})
      .to(".basket", {y: 60, x: 15, rotation: 25, duration: 1})
      .to(".egg", .10, {opacity: 1, zindex:0, transformOrigin: "50% 100%", scaleY:0.25, yoyo:true})
      .to(".egg", .50, {rotation:90, transformOrigin:"left 50%", x: -40, transformOrigin: "50% 100%", ease: "out", y: -25})
      
      //Rabbit hop animation
      .to(".rabbit1foot", 0.8, {
        rotate:"-10_ccw",
        translateY:"1.5px",
        yoyo:true,
        repeat: -1 
        })

      .to(".rabbit1torso", 0.8, {
        translateY:"1px",
        yoyo:true,
        repeat: -1
        })

    //Rabbit1  moves into frame
      .to(".rabbit1", 1.8, {
          y: 45, 
          x: 315})
    
    //rabbit1 notices egg.
      .to(".rabbit1head", 1, {
          rotate:-10,
          translateY:"1.8px",
          translateX:"-2.5px"   
        })
      
      .to(".rabbit1head", 0.4, {
          rotate:0,
          translateY:"0px",
          translateX:"0px" 
        })

    //Rabbit1 moves to egg.
      .to(".rabbit1", 3.5, {
        y: 85, 
        x:200, 
        scale:1})
      .to(".rabbit1ear", 0.5, {
          rotate:2,
          yoyo:true,
          repeat:-1

        })
      .to(".rabbit1head", 0.5, {
          rotate:-10,
          translateY:"1.8px",
          translateX:"-2.5px"   
      })
      .to(".rabbit1", 0.5, {
          x:195
      })
      
      //Rabbit1 picks up Egg.
      .to(".rabbit1arms", 0.2, {
          translateX:"-2.5px"
      })
      .to(".rabbit1arms", 0.2, {
          rotate:-2
      })
      .to(".egg", 0.2, {
        rotate:150,
        translateY:"-45px",
        translateX:"-30px",
    })
    .to(".rabbit1arms", 0.2, {
        rotate:60,
        translateY:"-3.5px",
        translateX:"1.5px"
    })
    .to(".rabbit1ear", 0.2, {
        rotate:0,
    })
    .to(".rabbit1head", 0.2, {
        rotate:0,
        translateY:"0px",
        translateX:"0px" 
    })

    //Rabbit1 holding egg positioned.
    .to(".rabbit1egg", {
        
        y:85,
        x:195,
        opacity:0,
    })
    .to(".rabbit1egg", 0.1, {
        opacity:1,
    })
    .to(".rabbit1", 0, {
        opacity:0,
    })
    .to(".egg", 0, {
        opacity:0,
    })

    //Rabbit1 holding egg hops away.
    .to(".rabbit1egg", 0.2, {
        translateY:"70px",
        yoyo:true,
        repeat:7
    })
    .to(".rabbit1egg", 2, {
        x:10,   
    })

    //Egg basket positioned.
    .to(".basket", 0, {
        opacity:0
    })

    //Background 2 displayed.
    .to('#bg2', 0, {
        opacity:1
    })

    //Rabbit 1 with egg positioned
    .to(".rabbit1egg", 0, {
        opacity:0,
        x:350
    })
    .to(".rabbit1egg", 0, {
        opacity:1
    })

    //Rabbit 2 and the baskt of eggs appears
    .to(".rabbit2", 0.2, {
        
        opacity:1
    })
    .to(".eggbasket", 0.2, {
        opacity:1
    })

    //Rabbit 1 with egg enters frame
    .to(".rabbit1egg", 0.2, {
        translateY:"70px",
        yoyo:true,
        repeat:5
    })
    .to(".rabbit1egg", 1.5, {
        x:270,
    })
    .to(".rabbit1egg", {
        translateY:"90px",
    })

    //Rabbit 2 approaches
    .to(".rabbit2", 0.2, {
        translateY:"25px",
        yoyo:true,
        repeat:5
    })
    .to(".rabbit2", 1.5, {
        x:10
    })

    //Rabbit 2 puts note on Egg
    .to(".rabbit2arms", 0.5, {
        rotate:-20,
        translateX:"2.5px"
    })
    .to(".paper", 0.5, {
        opacity:1
    })
    .to(".rabbit2arms", 0.5, {
        rotate:20,
        translateX:"-2.5px"
    })

    // Note appears, h3 containing bespoke information appears.
    .to(".note", 0.5, {
        opacity:1
    })
    .to("h3", 0.2, {
        opacity:1
    })
    .to(".eggbasket", 0, {
        opacity:0
    })
   

    
    
    
    
      
      


       
      
      

